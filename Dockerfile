FROM ubuntu:16.04
RUN apt-get update && apt-get upgrade -y
RUN apt-get install git -y
WORKDIR /libs
RUN git clone https://github.com/protobuf-c/protobuf-c.git
WORKDIR /libs/protobuf-c
RUN git checkout v1.3.2
RUN apt-get install -y autoconf libtool g++ pkg-config
COPY --from=registry.gitlab.com/loranna/libprotobuf-builder:latest /usr/local/lib /usr/local/lib
COPY --from=registry.gitlab.com/loranna/libprotobuf-builder:latest /usr/local/bin /usr/local/bin
COPY --from=registry.gitlab.com/loranna/libprotobuf-builder:latest /usr/local/include /usr/local/include
RUN ldconfig
RUN ./autogen.sh && ./configure && make -j4 && make install
RUN ldconfig
ENTRYPOINT [ "/bin/bash" ]
